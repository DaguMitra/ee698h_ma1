classdef CRC_CLASS
    properties
        K = 319784;      % Length of a dataword (b)
        G;      % Length of the generator (b)
        N;      % Length of a codeword (b)
        
        GENERATOR_1 = [1 1 0 0 0 0 1 1 0 0 1 0 0 1 1 0 0 1 1 1 1 1 0 1 1];      % CRC24A as per the standard
        GENERATOR_2 = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1];      % CRC24B as per the standard
        GENERATOR_3 = [1 1 0 1 1 0 0 1 0 1 0 1 1 0 0 0 1 0 0 0 1 0 1 1 1];      % CRC24C as per the standard
        GENERATOR;      % Selected Generator
    end
    
    methods
        % ==================================================Constructor
        function [obj] = CRC_CLASS()
            %{
            fprintf("Generator 1 = \n");
            disp(obj.GENERATOR_1);
            fprintf("Generator 2 = \n");
            disp(obj.GENERATOR_2);
            fprintf("Generator 3 = \n");
            disp(obj.GENERATOR_3);
            %}
            
            generator_choice = input('Select a 24 bits generator (1 / 2 / 3 / DEFAULT : 1): ');

            switch generator_choice
                case 1
                    obj.GENERATOR = obj.GENERATOR_1;
                case 2
                    obj.GENERATOR = obj.GENERATOR_2;
                case 3
                    obj.GENERATOR = obj.GENERATOR_3;
                otherwise
                    obj.GENERATOR = obj.GENERATOR_1;
            end

            obj.G = size(obj.GENERATOR, 2);      % Length of the generator (b)
            obj.N = obj.G + obj.K - 1;      % Length of a codeword (b)
        end
        
        % ==================================================Generate A Dataword Randomly
        function [DATAWORD] = crc_generate_dataword_randomly(obj)
            DATAWORD = randi([0 1], 1, obj.K);
        end
        
        % ==================================================CRC Encoder
        function [CODEWORD] = crc_encoder(obj, DATAWORD)
            AUGMENTED_DATAWORD = [DATAWORD, zeros(1, (obj.N - obj.K))];      % Augment (N - K) bits of 0s to the DATAWORD
            ZERO_GENERATOR = zeros(1, obj.G);

            for i = 1:obj.K
                if AUGMENTED_DATAWORD(i) == 1       % Quotient = 1
                    REMAINDER = xor(AUGMENTED_DATAWORD(i:(i + obj.N - obj.K)), obj.GENERATOR);
                else        % Quotient = 0
                    REMAINDER = xor(AUGMENTED_DATAWORD(i:(i + obj.N - obj.K)), ZERO_GENERATOR);
                end 

                AUGMENTED_DATAWORD(i:(i + obj.N - obj.K)) = REMAINDER;      % Inplace Updation
            end

            CODEWORD = [DATAWORD, REMAINDER(2:obj.G)];     % Append the REMAINDER to the DATAWORD
        end
        
        % ==================================================Introduce Error in the Codeword Randomly
        function [MODIFIED_CODEWORD] = crc_introduce_error_randomly(obj, CODEWORD)
            rng('shuffle');
            p = rand;       % Probability

            if p <= 0.5     % Modify the CODEWORD
                for i = 1:obj.N
                    p = rand;

                    if p <= 0.5     % Flip the bit
                        CODEWORD(i) = not(CODEWORD(i));
                    end
                end
            end

            MODIFIED_CODEWORD = CODEWORD;
        end
        
        % ==================================================CRC Decoder
        function [decision, DATAWORD] = crc_decoder(obj, MODIFIED_CODEWORD)
            MODIFIED_CODEWORD_TEMP = MODIFIED_CODEWORD;
            ZERO_GENERATOR = zeros(1, obj.G);

            for i = 1:obj.K
                if MODIFIED_CODEWORD_TEMP(i) == 1       % Quotient = 1
                    REMAINDER = xor(MODIFIED_CODEWORD_TEMP(i:(i + obj.N - obj.K)), obj.GENERATOR);
                else        % Quotient = 0
                    REMAINDER = xor(MODIFIED_CODEWORD_TEMP(i:(i + obj.N - obj.K)), ZERO_GENERATOR);
                end 

                MODIFIED_CODEWORD_TEMP(i:(i + obj.N - obj.K)) = REMAINDER;      % Inplace Updation
            end

            SYNDROME = REMAINDER(2:obj.G);

            if any(SYNDROME)        % Discard
                decision = 0;
            else        % Accept
                decision = 1;        
            end

            DATAWORD = MODIFIED_CODEWORD(1:obj.K);
        end
    end
end