crc_obj = CRC_CLASS();      % Object of CRC_CLASS            
            
[DATAWORD] = crc_obj.crc_generate_dataword_randomly();
%fprintf("Generated Dataword = \n");
%disp(DATAWORD);

[CODEWORD] = crc_obj.crc_encoder(DATAWORD);
%fprintf("Sent Codeword = \n");
%disp(CODEWORD);

[MODIFIED_CODEWORD] = crc_obj.crc_introduce_error_randomly(CODEWORD);
%fprintf("Received Codeword = \n");
%disp(MODIFIED_CODEWORD);

[decision, DATAWORD] = crc_obj.crc_decoder(MODIFIED_CODEWORD);

if decision == 0
    disp("0");
else
    disp("1");
    %fprintf("Received Dataword = \n");
    %disp(DATAWORD);
end